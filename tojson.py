#!/usr/bin/python3
# -*- coding: utf-8 -*-
import smil
import sys
import json


class ElementJSON(smil.Element):

    def __init__(self, nombre, dicc_atributos):
        """Método iniciliazador"""
        self.nombre = nombre
        self.dicc_atributos = dicc_atributos

    def dict(self):
        dicc = {}
        objeto = smil.Element(self.nombre, self.dicc_atributos)
        dicc['name'] = objeto.name()
        dicc['attrs'] = objeto.attrs()
        return dicc


class SMILJSON(smil.SMIL):

    def __init__(self, path):
        """Método iniciliazador"""
        self.path = path

    def json(self):
        objeto2 = smil.SMIL(self.path)
        elementos = objeto2.elements()
        lista_diccionarios = []
        for elemento in elementos:
            objeto3 = ElementJSON(elemento[0],elemento[1])
            diccionario = (objeto3.dict())
            lista_diccionarios.append(diccionario)
        return json.dumps(lista_diccionarios, indent = 2)


def main():

    if len(sys.argv) > 1:
        path = sys.argv[1]
        objeto4 = SMILJSON(path)
        print(objeto4.json())

    else:
        print("Usage: python3 tojson.py <file>")
        exit()


if __name__ == "__main__":
    main()
