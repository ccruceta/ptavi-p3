#!/usr/bin/python3
# -*- coding: utf-8 -*-

"""Simple program to parse a chistes XML file"""
import sys
import xml.dom.minidom


class Humor:

    def __init__(self, path):
        """Método iniciliazador"""
        self.path = path

    def jokes(self):
        """"""
        document = xml.dom.minidom.parse(self.path)
        root_element = document.documentElement.tagName

        if root_element != "humor":
            raise Exception("Root element is not humor.")

        jokes = document.getElementsByTagName('chiste')

        jokes_list = []
        scores = ['buenisimo', 'bueno', 'regular', 'malo', 'malisimo']
        jokes_list_ordenada = []

        for joke in jokes:
            joke_diccionary = {'score': '', 'question': '', 'answer': ''}
            joke_diccionary['score'] = joke.getAttribute('calificacion')
            questions = joke.getElementsByTagName('pregunta')
            joke_diccionary['question'] = questions[0].firstChild.nodeValue.strip()
            answers = joke.getElementsByTagName('respuesta')
            joke_diccionary['answer'] = answers[0].firstChild.nodeValue.strip()
            jokes_list.append(joke_diccionary)

        for i in scores:
            for joke in jokes_list:
                if joke['score'] == i:
                    jokes_list_ordenada.append(joke)

        return jokes_list_ordenada


def main(path):

    objeto = Humor(path)
    lista_chistes = objeto.jokes()

    for chiste in lista_chistes:
        print(f"Calificación: {chiste['score']}.")
        print(f" Pregunta: {chiste['question']}")
        print(f" Respuesta: {chiste['answer']}\n")


if __name__ == "__main__":

    path = sys.argv[1]
    main(path)

