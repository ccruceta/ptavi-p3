#!/usr/bin/python3
# -*- coding: utf-8 -*-

import xml.dom.minidom
import sys
import urllib.request


def main():

    if len(sys.argv) > 1:
        path = sys.argv[1]
        documento = xml.dom.minidom.parse(path)
        elementos = documento.getElementsByTagName('*')
        n = 0
        for elemento in elementos:
            if elemento.hasAttribute('src'):
                atributos = elemento.attributes
                valor_atributo = atributos.item(0).value
                if valor_atributo.startswith('http://'):
                    print(valor_atributo)
                    ext = valor_atributo.split('.')[-1]
                    filename = "fichero-"+str(n)+"."+ext
                    print(filename)
                    urllib.request.urlretrieve(valor_atributo, filename)
                    n += 1

    else:
        print("Usage: python3 download.py <file>")
        exit()


if __name__ == "__main__":
    main()
