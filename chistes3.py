#!/usr/bin/python3
# -*- coding: utf-8 -*-

import sys
import xml.dom.minidom


def main(path):
    """Programa principal"""

    document = xml.dom.minidom.parse(path)
    root_element = document.documentElement.tagName
    if root_element != "humor":
        raise Exception("Root element is not humor.")

    neat_jokes = []

    scores = ['buenisimo', 'bueno', 'regular', 'malo', 'malisimo']

    jokes = document.getElementsByTagName('chiste')

    for i in scores:
        for joke in jokes:
            score = joke.getAttribute('calificacion')
            if score == i:
                neat_jokes.append(joke)

    for joke in neat_jokes:
        score = joke.getAttribute('calificacion')
        questions = joke.getElementsByTagName('pregunta')
        question = questions[0].firstChild.nodeValue.strip()
        answers = joke.getElementsByTagName('respuesta')
        answer = answers[0].firstChild.nodeValue.strip()
        print(f"Calificación: {score}.")
        print(f" Pregunta: {question}")
        print(f" Respuesta: {answer}\n")


if __name__ == "__main__":

    path = sys.argv[1]
    main(path)
