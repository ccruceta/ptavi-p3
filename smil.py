#!/usr/bin/python3
# -*- coding: utf-8 -*-

import xml.dom.minidom


class SMIL:

    def __init__(self, path):
        """Método iniciliazador"""
        self.path = path

    def elements(self):
        """Creamos una lista con los elementos"""
        lista_elementos = []
        documento = xml.dom.minidom.parse(self.path)
        elementos = documento.getElementsByTagName('*')

        for elemento in elementos:
            lista_elemento = []
            nombre = elemento.tagName
            lista_elemento.append(nombre)
            atributos = elemento.attributes
            dicc_atributos = {}
            for i in range(atributos.length):
                valor_atributo = atributos.item(i).value
                nombre_atributo = atributos.item(i).name
                dicc_atributos[nombre_atributo] = valor_atributo
            lista_elemento.append(dicc_atributos)
            lista_elementos.append(lista_elemento)

        return lista_elementos


class Element:

    def __init__(self, nombre_elemento, dicc_atributos):
    
        self.nombre_elemento = nombre_elemento
        self.dicc_atributos = dicc_atributos

    def name(self):

        return self.nombre_elemento

    def attrs(self):

        return self.dicc_atributos


